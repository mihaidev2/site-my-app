<?php

namespace App\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;


/**
 * @Template()
 */
class ArticlesController extends AbstractController
{
    /**
     * @Route("/articles", name="adm_articles_list")
     */
    public function list()
    {
        return [
            'name' => 'Mihai Constantinescu va trai'
        ];
    }

    /**
     * @Route("/articles/edit/{id}", name="adm_articles_edit")
     * @Route("/articles/add", name="adm_artiles_add")
     */
    public function edit($id=0)
    {
        return [
            'id' => $id,
        ];
    }
}