<?php

namespace App\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Template()
 */
class CategoriesController extends AbstractController
{
    /**
     * @Route("/categories", name="adm_categories_list")
     */
    public function list()
    {
        return [
            'name' => 'Mihai'
        ];
    }

    /**
     * @Route("/categories/edit/{id}", name="adm_categories_edit")
     * @Route("/categories/add", name="adm_categories_add")
     */
    public function edit($id=0)
    {
        return [
            'id' => $id
        ];
    }
}